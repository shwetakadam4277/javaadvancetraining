package ProductSearch;

import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class TshirtMain {

	public static void main(String[] args) throws FileNotFoundException {

		Csv csv = new Csv();
		Thread t1 = new Thread(csv);
		t1.start();

		// taking user inputs

		Scanner sc = new Scanner(System.in);
		try {

			System.out.print("Enter Colour of uT-shirt  : ");
			String colour = sc.nextLine().toUpperCase();
			System.out.print("Enter Size(S, M, L, XL, XXL)  : ");
			String size = sc.nextLine().toUpperCase();
			System.out.print("Enter Gender for t-shirt(M,F,U)  : ");
			String gender = sc.nextLine().toUpperCase();
			System.out.print(
					"Enter Output Preference :   1. Price \t 2. Rating \t 3. Price and Rating \nEnter Preference Choice Code : ");
			int outputPreference = sc.nextInt();
			sc.close();

			// operations on inputs
			TshirtOperations pc = new TshirtOperations();

			pc.search(csv.productList(), colour, size, gender);
			pc.outputChange(outputPreference);

		} catch (Exception e) {
			System.out.println("Enter a number in Integer Format " + e);
		}

	}

}
