package ProductSearch;

import java.util.ArrayList;

public class TshirtDisplay {
	public void TshirtDisplay(ArrayList<TshirtDetails> arr) {
//		display the data
		System.out.println("***** PRODUCT INFORMATION *****");
		System.out.println("Id|Name|Color|Gender| Size|Price|Rating|Availability");
		for (TshirtDetails p : arr) {
			System.out.print("| " + p.getId());
			System.out.print(" |" +p.getName());
			System.out.print( "| " +p.getColor());
			System.out.print( "| " +p.getGender());
			System.out.print("| " + p.getSize());
			System.out.print("| " + p.getPrice());
			System.out.print(" |" + p.getRating());
			System.out.print(" |" + p.getAvailability());
			System.out.println("\n");
		}
		if (arr.isEmpty()) {
			System.out.println("T-shirts  Are Not Available.");
		}
	}

	
}
