package ProductSearch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.List;

public class TshirtOperations {

	ArrayList<TshirtDetails> arr = new ArrayList<TshirtDetails>();
	TshirtDisplay show = new TshirtDisplay();
	protected TshirtDetails r1;
	protected TshirtDetails r2;

	

	public void outputChange(int outputPreference) {

		if (outputPreference == 1) {

			List<TshirtDetails> tshirtList = arr;
			Collections.sort(tshirtList, new Comparator<TshirtDetails>() {
				public int compare(TshirtDetails p1, TshirtDetails p2) {
					return (int) (p1.getPrice() - p2.getPrice());
				}
			});
		}

		else if (outputPreference == 2) {
			List<TshirtDetails> tshirtList = arr;
			Collections.sort(tshirtList, new Comparator<TshirtDetails>() {
				public int compare(TshirtDetails r1, TshirtDetails r2) {
					return (int) (r1.getRating() - r2.getRating());
				}
			});

		}

		else if (outputPreference == 3) {
			List<TshirtDetails> tshirtList = arr;
			Collections.sort(tshirtList, new Comparator<TshirtDetails>() {
				public int compare(TshirtDetails p1, TshirtDetails p2) {
					double flag = (double) (p1.getPrice() - p2.getPrice());
					if (flag == 0)
						flag = Float.compare(r1.getRating(), r2.getRating());

					return (int) flag;
				}
			});
		}

		else {
			System.out.println("Wrong Choice");
			return;
		}

		show.TshirtDisplay(arr);

	}
	public void search(List<TshirtDetails> tshirtList, String color, String size, String gender) {
		for (TshirtDetails d : tshirtList) {
			if (d.getColor().equalsIgnoreCase(color) && d.getSize().equalsIgnoreCase(size)
					&& d.getGender().equalsIgnoreCase(gender)) {
				arr.add(d);
			}
		}

	}

}
