package ProductSearch;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

// 	this class is used to fetch the data from the csv file 
public class Csv implements Runnable {

	List<TshirtDetails> list = new ArrayList<TshirtDetails>();
	List<TshirtDetails> productList;

	 
	public List<TshirtDetails> getArray() {
		CSVParser csvParser = new CSVParserBuilder().withSeparator('|').build();

		File dir = new File("C:\\Users\\shwetakadam\\eclipse-workspace\\JavaAdvancedTraining\\CSVDataFile");
		File[] file = dir.listFiles();
		for (File f : file) {
			System.out.println(f.getName());
			try {
				CSVReader csvReader = new CSVReaderBuilder(new FileReader(f.getAbsolutePath())).withCSVParser(csvParser)
						.withSkipLines(1).build();

				String[] record = null;
				while ((record = csvReader.readNext()) != null) {
					TshirtDetails pm = new TshirtDetails(record[0], record[1], record[2], record[3], record[4],
							Float.parseFloat(record[5]), Float.parseFloat(record[6]), record[7]);
					list.add(pm);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return list; // return data in a list
	}

	public void run() {
		productList = getArray();
	}

	public List<TshirtDetails> productList() {
		return productList;

	}

}
